﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIBubble : MonoBehaviour {

    public GameObject uiBuildChoiceGameObject;
    public GameObject uiBuildSelectorGameObject;
    public Sprite[] uiBuildChoicesImages;
    private bool buildMode = false;

    public Unit unit;

    private Vector3 tPos;
    private Vector3 pos;
    private float tScale = 1;
    private float scale = 1;


    GameObject[] uiBuildChoices;
    Vector3[] uiBuildChoicesTPos;

    public int selectedIndex = -1;

    private void Start() {
        if (unit) {
            this.transform.GetChild(0).GetComponent<Text>().color = GM.playerColors[unit.player - 1];
        }
    }
    private void Update() {
        //Position lerp
        pos = Vector3.Lerp(pos, tPos, Time.deltaTime * 15f);
        this.transform.position = GM.camera.WorldToScreenPoint(pos);
        //Scale lerp
        scale = Mathf.Lerp(scale, tScale, Time.deltaTime * 15f);
        this.transform.localScale = new Vector3(scale, scale, scale);

        if (buildMode) {
            selectedIndex = -1;
            Vector3 stickDirection = new Vector3(Input.GetAxis("Horizontal" + unit.controller), Input.GetAxis("Vertical" + unit.controller), 0f);
            if (stickDirection.magnitude > 0.3f) {
                float angle = Vector3.SignedAngle(Vector3.up, stickDirection, -Vector3.forward);
                angle = (angle >= 0) ? angle : angle + 360;
                angle += 180/ uiBuildChoices.Length;
                selectedIndex = (int)Mathf.Floor((angle%360) / 360f * uiBuildChoices.Length);
            }

            for (int i = 0; i < uiBuildChoices.Length; i++) {
                float tScale = 1;
                if (i == selectedIndex) {
                    tScale = 1.5f;
                }
                uiBuildChoices[i].transform.localScale = Vector3.Lerp(uiBuildChoices[i].transform.localScale,new Vector3(tScale, tScale, tScale),Time.deltaTime*25f);
            }

            if (selectedIndex >= 0) {
                SetText(Building.BuildingCosts[selectedIndex+1].ToString());
                unit.ShowBuildingInformation((BuildingType)(selectedIndex+1));
            }
        }



    }

    public void MoveLerp(Vector3 worldPosition) {
        tPos = worldPosition;
    }

    public void SetPosition(Vector3 worldPosition) {
        tPos = worldPosition;
        pos = tPos;
    }

    public void Show() {
        //Funciona?
        this.gameObject.SetActive(true);
    }

    public void Hide() {
        this.gameObject.SetActive(false);
    }

    public void SetText(string text) {
        this.transform.GetChild(0).GetComponent<Text>().text = text;
    }

    public void SetMode(string mode) {
        switch (mode) {
            case "build":
                SetText("X");
                tScale = 0.6f;
                break;
            case "buildMode":
                SetText("");
                tScale = 1.1f;
                OpenBuildMode();
                break;
            case "use":
                if(unit.focusedTile.building.type == BuildingType.Mine) {
                    if (((MineBuilding)unit.focusedTile.building).IsReady()) {
                        SetText("A");
                    } else {
                        SetText("...");
                    }
                }
                SetText("A");
                tScale = 0.6f;
                break;
            case "attack":
                SetText("Y");
                tScale = 0.6f;
                break;
            case "hidden":
                SetText("");
                tScale = 0.0f;
                break;
            case "attackNoMana":
                SetText(" ");
                tScale = 0.8f;
                break;
            default:
                SetText(" ");
                tScale = 1f;
                break;
        }
    }

    public void OpenBuildMode() {
        if (!buildMode) {
            buildMode = true;
            int n = (int)BuildingType.Count - 1; //-1 porque no se pueden construir depósitos de cristal
            float radius = 36f;
            uiBuildChoices = new GameObject[n];
            for (int i = 0; i < uiBuildChoices.Length; i++) {
                uiBuildChoices[i] = (GameObject)Instantiate(uiBuildChoiceGameObject, this.transform);
                float angle = i / (float)n * 360;
                Vector3 pos = Quaternion.AngleAxis(angle, -Vector3.forward) * Vector3.up * radius;
                uiBuildChoices[i].transform.localPosition = pos;
                uiBuildChoices[i].transform.GetChild(0).GetComponent<Image>().sprite = uiBuildChoicesImages[i];
                uiBuildChoices[i].transform.GetChild(0).GetComponent<Image>().color = GM.playerColors[unit.player-1];
            }
        }
    }

    public void CloseBuildMode() {
        if (buildMode) {
            buildMode = false;
            for (int i = 0; i < uiBuildChoices.Length; i++) {
                Destroy(uiBuildChoices[i]);
            }
        }
    }
}
