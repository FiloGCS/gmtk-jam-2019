﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerPanel : MonoBehaviour {

    public TextMeshProUGUI crystalsRef;
    public TextMeshProUGUI healthRef;
    public Image healthBarRef;
    public Image healthBarDeltaRef;
    public Text infoTitleRef;
    public Text infoBodyRef;
    public Image avatarRef;
    public Image infoSpriteRef;


    public Unit unit;
    public float crystals;
    public float health;
    public string infoTitle;
    public string infoBody;
    public Sprite avatar;
    public Sprite infoSprite;

    private void Update() {
        if (unit) {
            crystalsRef.text = unit.crystals.ToString("0");
            healthRef.text = unit.health.ToString("0");
            if (healthBarRef) {
                float value = Mathf.Lerp(0, 105, unit.health / unit.maxHealth);
                healthBarRef.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, value);
                float currentDeltavalue = healthBarDeltaRef.GetComponent<RectTransform>().rect.height;
                if (currentDeltavalue > value) {
                    healthBarDeltaRef.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, currentDeltavalue-20*Time.deltaTime);
                }
            }
        } else {
            if (crystalsRef) {
                crystalsRef.text = crystals.ToString("0");
            }
            if (healthRef) {
                healthRef.text = health.ToString("0");
            }
        }
        if (infoTitleRef) {
            infoTitleRef.text = infoTitle;
        }
        if (infoBodyRef) {
            infoBodyRef.text = infoBody;
        }

        if (avatar) {
            avatarRef.sprite = avatar;
        }
        if (infoSprite) {
            infoSpriteRef.sprite = infoSprite;
        }

    }

}

