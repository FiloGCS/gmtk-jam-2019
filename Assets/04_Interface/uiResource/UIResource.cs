﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIResource : MonoBehaviour {

    public void SetText(string text) {
        this.GetComponentInChildren<TextMeshProUGUI>().text = text;
    }

    public void Kill() {
        Destroy(this.gameObject);
    }
}
