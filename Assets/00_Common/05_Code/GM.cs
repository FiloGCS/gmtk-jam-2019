﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM : MonoBehaviour {
    
    public static Camera camera;
    public static Map map;
    public static Canvas canvas;
    public static PlayerPanel[] playerPanels = new PlayerPanel[2];
    public static GM gm;
    public static Unit[] playerUnits = new Unit[2];
    public static Color[] playerColors = { new Color(0.3f,0.5f,0.8f,1f), new Color(0.8f, 0.3f, 0.3f,1f) };
    
    public static Unit GetOtherPlayer(int player) {
        if(player == 1) {
            return playerUnits[1];
        } else {
            return playerUnits[0];
        }
    }

    private void Awake() {
        gm = this;
        camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        map = GameObject.Find("Map").GetComponent<Map>();
        canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        playerPanels[0] = GameObject.Find("Player Panel 1").GetComponent<PlayerPanel>();
        playerPanels[1] = GameObject.Find("Player Panel 2").GetComponent<PlayerPanel>();
    }
}
