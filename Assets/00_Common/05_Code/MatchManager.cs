﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MatchManager : MonoBehaviour {
    public List<int> controllers;
    public GameObject P1UnitGameObject;
    public GameObject P2UnitGameObject;

    public GameObject P1;
    public GameObject P2;

    public GameObject MatchPanel;
    public Text P1ReadyText;
    public Text P2ReadyText;

    public GameObject endMatchPanel;
    public static Unit loser;

    private bool waitingForReady = true;

    // Start is called before the first frame update
    void Start() {
        controllers = new List<int>();
    }

    // Update is called once per frame
    void Update() {

        for (int i = 1; i < 8; i++) {
            if (Input.GetKeyDown("joystick " + i + " button 0") && !controllers.Contains(i)) {
                print("Controller " + i + " joined");
                controllers.Add(i);
                if (controllers.Count >= 2) {
                    StartMatch();
                }
            }
        }
        if (controllers.Count >= 1) {
            P1ReadyText.text = "Player 1 Ready";
        }
        if (controllers.Count >= 2) {
            P1ReadyText.text = "Player 2 Ready";
        }

    }

    void StartMatch() {
        print("STARTING MATCH");
        waitingForReady = false;

        P1 = (GameObject)Instantiate(P1UnitGameObject, new Vector3(-2, 0, 0), Quaternion.identity);
        P1.GetComponent<Unit>().controller = controllers[0];
        P1.GetComponent<Unit>().player = 1;

        P2 = (GameObject)Instantiate(P2UnitGameObject, new Vector3(2, 0, 0), Quaternion.identity);
        P2.GetComponent<Unit>().controller = controllers[1];
        P2.GetComponent<Unit>().player = 2;
        MatchPanel.SetActive(false);
    }

    public void showEndPanel(Unit loser) {
        endMatchPanel.SetActive(true);
        endMatchPanel.transform.GetChild(loser.player - 1).gameObject.SetActive(true);
        StartCoroutine(DeferredLoadScene());
    }

    public static void EndMatch(Unit loser) {
        GM.gm.GetComponent<MatchManager>().showEndPanel(loser);
    }

    public IEnumerator DeferredLoadScene() {
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}
