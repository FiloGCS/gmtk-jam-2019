﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BuildingType {Crystal, Mine, WorkShop, MageTower, Count }

public class Building : MonoBehaviour {

    public static float[] BuildingCosts = { 999, 100, 100, 150};


    public BuildingType type;
    public Unit owner;
    public Tile tile;
    public bool usable = false;
    public List<GameObject> ColorableParts;

    public void Start() {

        foreach (GameObject part in ColorableParts) {
            part.GetComponent<Renderer>().material.SetColor("_Color", GM.playerColors[owner.player - 1]);
        }
    }
    public virtual bool CanBeUsedBy(Unit unit) {

        //Mines are public
        if (type == BuildingType.Crystal) {
            return true;
        }
        if(unit == owner) {
            return true;
        }
        if(unit.type == UnitType.thief) {
            return true;
        }
        return false;
    }
   
    public static Building Build(BuildingType buildingType, Tile tile, Unit owner) {
        if(buildingType == BuildingType.Crystal) {
            return null;
        }
        if (!tile.building) {
            GameObject newBuildingGameObject = null;
            if(owner.player == 1) {
                newBuildingGameObject = (GameObject)Instantiate(GM.map.BuildingsGameObjects1[(int)buildingType], tile.transform);
            }
            if(owner.player == 2) {
                newBuildingGameObject = (GameObject)Instantiate(GM.map.BuildingsGameObjects2[(int)buildingType], tile.transform);
            }
            Building newBuilding = newBuildingGameObject.GetComponent<Building>();
            newBuilding.tile = tile;
            tile.building = newBuilding;
            newBuilding.owner = owner;
            return newBuilding;
        }
        return null;
    }

    public virtual bool UseBuilding(Unit user) {
        return false;
    }

}
