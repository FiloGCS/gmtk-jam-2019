﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineBuilding : Building {
    public GameObject sphere;

    public ParticleSystem FullParticles;
    public ParticleSystem CollectingParticlesRef;
    public List<ParticleSystem> CollectingParticles;
    public GameObject UIReadyRef;
    private GameObject UIReady;
    public GameObject RotatingPiece;
    private float animationSpeed = 0f;

    public float maxCrystals = 50;
    public float crystals = 0;
    public List<Tile> neighbours;

    private Animator animator;

    new public void Start() {
        base.Start();
        animator = GetComponent<Animator>();
        UIReady = Instantiate(UIReadyRef, GM.canvas.transform);

        neighbours = GM.map.GetNeighbours(this.tile);
        //Spawn collecting particle systems
        foreach (Tile neighbour in neighbours) {
            if (neighbour.building && neighbour.building.type == BuildingType.Crystal) {
                Quaternion rot = Quaternion.LookRotation(neighbour.transform.position - this.transform.position);
                ParticleSystem newPS = Instantiate(CollectingParticlesRef, this.transform.position, rot);
                CollectingParticles.Add(newPS);
            }
        }
    }

    private void Update() {
        //Show particles if this building is ready
        if (IsReady()) {
            FullParticles.emissionRate = 20f;
            UIReady.transform.position = GM.camera.WorldToScreenPoint(this.transform.position + Vector3.up * 5f);
            UIReady.SetActive(true);
        } else {
            FullParticles.emissionRate = 0f;
            UIReady.SetActive(false);
        }

        float gains = 0;
        if (crystals < maxCrystals) {
            for (int i = 0; i < neighbours.Count; i++) {
                if (neighbours[i].building && neighbours[i].building.type == BuildingType.Crystal) {
                    gains += 3f;
                }
            }
            crystals += gains * Time.deltaTime;
            for (int i = 0; i < CollectingParticles.Count; i++) {
                CollectingParticles[i].emissionRate = 3f;
            }
        } else {
            for (int i = 0; i < CollectingParticles.Count; i++) {
                CollectingParticles[i].emissionRate = 0f;
            }
            crystals = maxCrystals;
        }

        //Play the mining animation
        animationSpeed = Mathf.Lerp(animationSpeed, gains, Time.deltaTime * 2);
        animator.SetFloat("speed", animationSpeed);

        float sphereScale = Mathf.Sqrt(crystals / maxCrystals);
        sphere.transform.localScale = Vector3.Lerp(sphere.transform.localScale, new Vector3(sphereScale, sphereScale, sphereScale), Time.deltaTime * 15f);
    }

    public override bool CanBeUsedBy(Unit unit) {
        if (!IsReady()) {
            return false;
        }
        return base.CanBeUsedBy(unit);
    }

    public override bool UseBuilding(Unit unit) {
        if (IsReady()) {
            base.UseBuilding(unit);
            unit.CollectCrystals(crystals);
            crystals = 0;
            this.GetComponent<AudioSource>().Play();
            return true;
        }
        return false;
    }

    public bool IsReady() {
        return (crystals == maxCrystals);
    }

}
