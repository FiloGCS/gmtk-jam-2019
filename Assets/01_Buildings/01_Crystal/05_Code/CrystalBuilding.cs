﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalBuilding : Building {

    public override bool UseBuilding(Unit unit) {
        base.UseBuilding(unit);
        unit.CollectCrystals(1);
        this.GetComponent<AudioSource>().pitch = Random.Range(0.9f, 1.15f);
        this.GetComponent<AudioSource>().Play();
        return true;
    }

    public void Start() {
        base.Start();
        int x = Random.Range(0, 3);
        this.transform.rotation = Quaternion.Euler(new Vector3(0, x * 90f, 0));
    }
}
