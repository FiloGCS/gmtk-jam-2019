﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkshopBuilding : Building {

    public override bool UseBuilding(Unit unit) {
        if (unit.type != UnitType.drone) {
            base.UseBuilding(unit);
            unit.ChangeType(UnitType.drone);
            return true;
        }
        return false;
    }
   
}
