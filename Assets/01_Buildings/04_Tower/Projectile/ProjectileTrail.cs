﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileTrail : MonoBehaviour {
    public AudioClip ExplosionAudio;

    public void Explode() {
        this.GetComponent<AudioSource>().pitch = Random.Range(0.5f, 0.7f);
        this.GetComponent<AudioSource>().PlayOneShot(ExplosionAudio);
        StartCoroutine(WaitAndKill());
    }

    IEnumerator WaitAndKill() {
        yield return new WaitForSeconds(3f);
        Destroy(this.gameObject);
    }

}
