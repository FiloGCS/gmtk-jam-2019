﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public GameObject AreaMarkerGameObject;
    public GameObject ExplosionParticlesGameObject;
    public GameObject myTrail;

    public Unit target;
    public Unit owner;
    public Vector3 origin;
    public Vector3 targetPosition;
    public float speed = 30f;
    public float explosionRadius = 4f;
    public float spread = 2f;
    public float damage = 50f;
    public GameObject areaMarker;
    public bool traveling = false;
    private Material areaMarkerMaterial;

    public void StartTraveling(Vector3 origin, Unit target, Unit owner) {
        this.target = target;
        this.owner = owner;
        this.origin = origin;
        targetPosition = target.transform.position + new Vector3(Random.Range(-spread, spread), 0, Random.Range(-spread, spread));
        targetPosition.y = 0;
        areaMarker = (GameObject)Instantiate(AreaMarkerGameObject, targetPosition, Quaternion.identity);
        areaMarkerMaterial = areaMarker.GetComponentInChildren<Renderer>().material;
        areaMarkerMaterial.SetColor("_UnitColor", GM.playerColors[owner.player - 1]);
        traveling = true;
    }

    void Update() {
        if (traveling) {

            //Update position
            Vector3 v = targetPosition - transform.position;
            this.transform.position += v.normalized * Time.deltaTime * speed;

            //Update areaMarker
            areaMarkerMaterial.SetFloat("_Alpha", 1-(Vector3.Distance(this.transform.position, targetPosition) / Vector3.Distance(origin, targetPosition)));

            //Check for reached target
            if (this.transform.position.y <= 0.3f) {
                Explode();
            }
        }
    }

    public void Explode() {
        
        myTrail.transform.SetParent(null);
        myTrail.GetComponent<ProjectileTrail>().Explode();
        Destroy(areaMarker);
        Destroy(this.gameObject);
        //Spawn Explosion VFX
        Instantiate(ExplosionParticlesGameObject, targetPosition, Quaternion.identity);
        //Deal damage in area
        if (Vector3.Distance(targetPosition , target.transform.position) < explosionRadius) {
            target.Damage(50f);
        }
    }
}
