﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MageTowerBuilding : Building {

    public GameObject[] projectileGameObject;
    public Transform projectileSpawn;
    public AudioClip shotAudio;

    public override bool UseBuilding(Unit unit) {
        if (unit.type != UnitType.mage) {
            base.UseBuilding(unit);
            unit.ChangeType(UnitType.mage);
            return true;
        }
        return false;
    }

    public void Attack() {
        Unit target;
        if (this.owner.player == 1) {
            target = GM.playerUnits[1];
        } else {
            target = GM.playerUnits[0];
        }

        GameObject projectile = (GameObject)Instantiate(projectileGameObject[owner.player-1], projectileSpawn.transform.position, Quaternion.identity);
        projectile.GetComponent<Projectile>().StartTraveling(projectileSpawn.position, target, this.owner);

        this.GetComponent<AudioSource>().pitch = Random.Range(0.85f, 1.15f);
        this.GetComponent<AudioSource>().PlayOneShot(shotAudio);

    }
}
