﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.ootii.Input;
using UnityEngine.UI;

public enum UnitType { drone, mage, thief };

public class Unit : MonoBehaviour {
    //Input
    static float AXIS_SENSITIVITY = 0.05f;
    public int player = -1;
    public int controller = -1;
    //Movement
    public float baseSpeed = 5.0f;
    public float speed;
    public float rotationSpeed = 5.0f;
    //Building
    public List<Building> buildings;
    public GameObject[] TileMarkerGameObject;
    private GameObject tileMarker;
    public Tile focusedTile;
    public bool buildingMode = false;
    public GameObject buildingParticles;
    public AudioClip buildingAudio;

    //Interface
    public GameObject uiBubbleGameObject;
    public UIBubble bubble;

    //Resources
    public GameObject[] uiResourceGameObject;
    public float crystals = 100;
    public float attackCost = 15f;
    public float maxHealth = 1000f;
    public float health;
    public AudioClip ouchAudio;

    //Unit Type
    public Material[] unitMaterials;
    public Renderer unitModel;
    public Animator animator;
    public float pickingTime = 0;
    public UnitType type = UnitType.drone;
    public Material[] mageHatMaterials;
    public GameObject MageHat;

    public AudioClip stepSound;

    public Sprite[] droneAvatars;
    public Sprite[] mageAvatars;

    public Sprite spriteVacia;
    public Sprite spriteDeposito;
    public Sprite spriteMina;
    public Sprite spriteTorre;
    public Sprite spriteWorkshop;

    private void Awake() {
        //Set up stats
        speed = baseSpeed;
        health = maxHealth;
    }
    private void Start() {
        buildings = new List<Building>();
        //Set up references
        GM.playerUnits[player - 1] = this;
        //Visual
        unitModel.material = unitMaterials[player - 1];
        MageHat.GetComponentInChildren<Renderer>().material = mageHatMaterials[player - 1];
        bubble = ((GameObject)Instantiate(uiBubbleGameObject, GM.canvas.transform)).GetComponent<UIBubble>();
        bubble.unit = this;
        GM.playerPanels[player - 1].unit = this;
        //TileMarker
        tileMarker = (GameObject)Instantiate(TileMarkerGameObject[player-1], new Vector3(9999, 9999, 9999), Quaternion.identity);
        ChangeType(UnitType.drone);
    }
    private void Update() {

        //Animations
        animator.SetFloat("speed", this.GetComponent<CharacterController>().velocity.magnitude);
        if (pickingTime > 0) pickingTime -= Time.deltaTime;
        animator.SetFloat("picking", pickingTime);

        focusedTile = Map.GetTileAtPosition(this.transform.position + this.transform.forward * 2);
        if (focusedTile) {
            //Update tileMarker position
            tileMarker.SetActive(true);
            tileMarker.transform.position = focusedTile.transform.position;

            //Update tile information
            ShowTileInformation(focusedTile);

            //Check what can my class do with this tile
            switch (type) {
                case UnitType.drone:
                    DroneInteractions();
                    break;
                case UnitType.mage:
                    MageInteractions();
                    break;
                case UnitType.thief:
                    ThiefInteractions();
                    break;
                default:
                    break;
            }

        } else {
            //If the tile is null
            tileMarker.SetActive(false);
        }

        //Movement
        if (!buildingMode) {
            //Read the input direction
            Vector3 vInput = new Vector3(Input.GetAxis("Horizontal" + controller), 0, Input.GetAxis("Vertical" + controller));
            //Cap the input magnitude to 1
            vInput = vInput.magnitude > 1.0f ? vInput.normalized : vInput;
            vInput = Quaternion.AngleAxis(GM.camera.transform.rotation.eulerAngles.y, Vector3.up) * vInput;
            Vector3 v = vInput * speed;

            if (v != Vector3.zero) {
                this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(vInput, Vector3.up), Time.deltaTime * rotationSpeed * v.magnitude);
            }
            v -= Vector3.up * 10f;
            this.GetComponent<CharacterController>().Move(v * Time.deltaTime);
        }


    }

    public void ChangeType(UnitType newType) {

        //Change this unit's type
        this.type = newType;

        // Show/Hide Mage hat
        MageHat.SetActive(newType == UnitType.mage);

        if (newType == UnitType.drone) {
            GM.playerPanels[player - 1].avatar = droneAvatars[player - 1];
        }
        if (newType == UnitType.mage) {
            GM.playerPanels[player - 1].avatar = mageAvatars[player - 1];
            print("ASD");
        }

    }

    //Drone Interactions
    public void DroneInteractions() {
        //If the tile is empty
        if (!focusedTile.building) {
            //If no one is building on it
            if (!focusedTile.currentBuilder || focusedTile.currentBuilder == this) {
                OnDroneFocusingFreeTile();
            }

        } else if (focusedTile.building.CanBeUsedBy(this)) {
            //If the tile has a building I can use
            OnDroneFocusingAllyTile();
        } else {
            //If the tile has a building of an enemy
            OnDroneFocusingEnemyTile();
        }

    }
    public void OnDroneFocusingFreeTile() {
        if (type == UnitType.drone) {
            //If I'm a drone
            if (!buildingMode) {
                //If I'm NOT in building mode
                bubble.SetMode("build");
                bubble.MoveLerp(Vector3.Lerp(this.transform.position + Vector3.up * 3, focusedTile.transform.position, 0.6f));
                bubble.Show();
                //If I press X
                if (Input.GetKeyDown("joystick " + controller + " button 2")) {
                    //Enter building mode
                    buildingMode = true;
                    bubble.OpenBuildMode();
                    focusedTile.currentBuilder = this;
                }
            } else {
                //If I'm in building mode
                bubble.SetMode("buildMode");
                bubble.MoveLerp(focusedTile.transform.position + Vector3.up);
                bubble.Show();
                //If I press X
                if (Input.GetKeyDown("joystick " + controller + " button 2")) {
                    //Build a mine
                    Build((BuildingType)bubble.selectedIndex + 1);
                    //Exit building mode
                    buildingMode = false;
                    bubble.CloseBuildMode();
                    focusedTile.currentBuilder = null;
                }
                //If I press A
                if (Input.GetKeyDown("joystick " + controller + " button 0")) {
                    //Build a mine
                    Build((BuildingType)bubble.selectedIndex + 1);
                    //Exit building mode
                    buildingMode = false;
                    bubble.CloseBuildMode();
                    focusedTile.currentBuilder = null;
                }
                //If I press B
                if (Input.GetKeyDown("joystick " + controller + " button 1")) {
                    //Exit building mode
                    buildingMode = false;
                    bubble.CloseBuildMode();
                    focusedTile.currentBuilder = null;
                }
            }
        }
    }
    public void OnDroneFocusingAllyTile() {
        //If I can use this building
        //update uiBubble
        bubble.SetMode("use");
        bubble.MoveLerp(Vector3.Lerp(this.transform.position + Vector3.up * 3, focusedTile.transform.position, 0.6f));
        bubble.Show();
        //If I press A
        if (Input.GetKeyDown("joystick " + controller + " button 0")) {
            //Use building
            if (focusedTile.building.UseBuilding(this)) {
                PlayPickAnimation();
            }
        }
    }
    public void OnDroneFocusingEnemyTile() {

        bubble.SetMode("hidden");
    }

    public void MageInteractions() {
        //If the tile has a building I can use
        if (focusedTile.building && focusedTile.building.CanBeUsedBy(this) && focusedTile.building.type == BuildingType.WorkShop) {
            //The tile is a workshop
            OnDroneFocusingAllyTile();
        } else {
            OnMageReadyToAttack();
        }
    }
    public void OnMageReadyToAttack() {
        if (crystals >= attackCost) {
            bubble.SetMode("attack");
            bubble.MoveLerp(this.transform.position + Vector3.up * 2 + (GM.GetOtherPlayer(player).transform.position - this.transform.position).normalized);
            bubble.Show();
            //If I press Y
            if (Input.GetKeyDown("joystick " + controller + " button 3")) {
                //Invoke Attack
                List<MageTowerBuilding> myTowers = new List<MageTowerBuilding>();
                foreach (Building b in buildings) {
                    if (b is MageTowerBuilding) {
                        myTowers.Add((MageTowerBuilding)b);
                        ((MageTowerBuilding)b).Attack();
                    }
                }
                SpendCrystals(attackCost);
            }
        } else {
            bubble.SetMode("attackNoMana");
            bubble.MoveLerp(this.transform.position + Vector3.up * 3f);
            bubble.Show();
        }
    }

    public void ThiefInteractions() {
        DroneInteractions();
    }

    public void CollectCrystals(float amount) {
        this.crystals += amount;
        //UI Element
        GameObject uiResource = Instantiate(uiResourceGameObject[player - 1], GM.camera.WorldToScreenPoint(this.transform.position + Vector3.up + new Vector3(Random.Range(0, 1f), 0, Random.Range(0, 1f))), Quaternion.identity, GM.canvas.transform);
        uiResource.GetComponent<UIResource>().SetText("+" + amount.ToString("0"));
        float uiScale = Mathf.Clamp(amount/30f,1,3)*0.75f;
        uiResource.transform.localScale = new Vector3(uiScale, uiScale, uiScale);
    }
    public bool SpendCrystals(float amount) {
        if (this.crystals >= amount) {
            this.crystals -= amount;
            //UI Element
            GameObject uiResource = Instantiate(uiResourceGameObject[player - 1], GM.camera.WorldToScreenPoint(this.transform.position + Vector3.up + new Vector3(Random.Range(0, 1f), 0, Random.Range(0, 1f))), Quaternion.identity, GM.canvas.transform);
            uiResource.GetComponent<UIResource>().SetText("-" + amount.ToString("0"));
            return true;
        } else {
            return false;
        }
    }
    public bool CanAfford(BuildingType type) {
        return (this.crystals >= Building.BuildingCosts[(int)type]);
    }
    public void Build(BuildingType type) {
        //If I can afford this building
        if (CanAfford(type)) {
            //Ask the Building Class for a building in this tile
            Building newBuilding = Building.Build(type, focusedTile, this);
            if (newBuilding) {
                //If everything goes alright
                SpendCrystals(Building.BuildingCosts[(int)type]);
                this.buildings.Add(newBuilding);
                Instantiate(buildingParticles, newBuilding.transform.position, Quaternion.identity);
                this.GetComponent<AudioSource>().pitch = Random.Range(0.85f, 1.15f);
                this.GetComponent<AudioSource>().PlayOneShot(buildingAudio);
            }
        }
    }

    public void Damage(float amount) {
        health -= amount;
        if (health <= 0) {
            print("P" + player + " loses the game!");
            MatchManager.EndMatch(this);
        }
        this.GetComponent<AudioSource>().pitch = Random.Range(0.8f, 1.2f);
        this.GetComponent<AudioSource>().PlayOneShot(ouchAudio);
    }

    public void PlayPickAnimation() {
        pickingTime = 0.3f;
    }

    public void ShowTileInformation(Tile focusedTile) {

        if (!focusedTile.building) {
            GM.playerPanels[player - 1].infoTitle = "Empty Tile";
            GM.playerPanels[player - 1].infoBody = "Workers can build in this tile.";
            GM.playerPanels[player - 1].infoSprite = spriteVacia;
        } else {
            if (focusedTile.building.type == BuildingType.Crystal) {
                GM.playerPanels[player - 1].infoTitle = "Crystal Deposit";
                GM.playerPanels[player - 1].infoBody = "Crystals are needed for pretty much everything.";
                GM.playerPanels[player - 1].infoSprite = spriteDeposito;
            }
            if (focusedTile.building.type == BuildingType.Mine) {
                GM.playerPanels[player - 1].infoTitle = "Crystal Mine";
                GM.playerPanels[player - 1].infoBody = "Gathers crystals from surrounding mines that can be collected once the mine is full.";
                GM.playerPanels[player - 1].infoSprite = spriteMina;
            }
            if (focusedTile.building.type == BuildingType.WorkShop) {
                GM.playerPanels[player - 1].infoTitle = "WorkShop";
                GM.playerPanels[player - 1].infoBody = "Allows a unit to become a Worker again. Workers can build and gather crystals from deposits and mines.";
                GM.playerPanels[player - 1].infoSprite = spriteWorkshop;
            }
            if (focusedTile.building.type == BuildingType.MageTower) {
                GM.playerPanels[player - 1].infoTitle = "Magic Tower";
                GM.playerPanels[player - 1].infoBody = "Allows a unit to become a Mage. Mages can use crystals to invoke powerful projectiles.";
                GM.playerPanels[player - 1].infoSprite = spriteTorre;
            }
        }
    }

    public void ShowBuildingInformation(BuildingType type) {

        if (type == BuildingType.Crystal) {
            GM.playerPanels[player - 1].infoTitle = "Crystal Deposit";
            GM.playerPanels[player - 1].infoBody = "Crystals are needed for pretty much everything.";
            GM.playerPanels[player - 1].infoSprite = spriteDeposito;
        }
        if (type == BuildingType.Mine) {
            GM.playerPanels[player - 1].infoTitle = "Crystal Mine";
            GM.playerPanels[player - 1].infoBody = "Gathers crystals from surrounding mines that can be collected once the mine is full.";
            GM.playerPanels[player - 1].infoSprite = spriteMina;
        }
        if (type == BuildingType.WorkShop) {
            GM.playerPanels[player - 1].infoTitle = "WorkShop";
            GM.playerPanels[player - 1].infoBody = "Allows a unit to become a Worker again. Workers can build and gather crystals from deposits and mines.";
            GM.playerPanels[player - 1].infoSprite = spriteWorkshop;
        }
        if (type == BuildingType.MageTower) {
            GM.playerPanels[player - 1].infoTitle = "Magic Tower";
            GM.playerPanels[player - 1].infoBody = "Allows a unit to become a Mage. Mages can use crystals to invoke powerful projectiles.";
            GM.playerPanels[player - 1].infoSprite = spriteTorre;
        }
    }

    public void PlayStepSound() {
        this.GetComponent<AudioSource>().pitch = Random.Range(0.85f, 1.15f);
        this.GetComponent<AudioSource>().PlayOneShot(stepSound);
    }
}
