﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepSound : MonoBehaviour
{

    public AudioClip stepClip;

    public void Step() {
        this.GetComponent<AudioSource>().pitch = Random.Range(0.9f, 1.1f);
        this.GetComponent<AudioSource>().PlayOneShot(stepClip);
    }
}
