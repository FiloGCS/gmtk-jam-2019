﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour {
    public GameObject TileGameObject;
    public GameObject CrystalDepositGameObject;

    public List<GameObject> BuildingsGameObjects1;
    public List<GameObject> BuildingsGameObjects2;

    public Tile[,] tiles;

    public float tileSize;
    public int xCount;
    public int zCount;

    private void Start() {

        tiles = new Tile[xCount, zCount];

        for (int i = 0; i < xCount; i++) {
            for (int j = 0; j < zCount; j++) {
                GameObject newTile = (GameObject)Instantiate(TileGameObject, this.transform);
                float xPos = (i - (xCount / 2f) + 0.5f) * tileSize;
                float zPos = (j - (zCount / 2f) + 0.5f) * tileSize;
                newTile.transform.localPosition = new Vector3(xPos, 0, zPos);
                //HACK - Algunas tiles son depósitos
                if (Random.Range(0f, 1f) > 0.65f) {
                    GameObject newCrystalDeposit = (GameObject)Instantiate(CrystalDepositGameObject, newTile.transform);
                    newTile.GetComponent<Tile>().building = newCrystalDeposit.GetComponent<Building>();
                }
                newTile.GetComponent<Tile>().map = this;
                tiles[i, j] = newTile.GetComponent<Tile>();
            }
        }
    }


    public static Tile GetTileAtPosition(Vector3 position) {
        Vector3 rayOrigin = new Vector3(position.x, 5, position.z);
        Ray ray = new Ray(rayOrigin, Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, LayerMask.GetMask("Tiles"))) {
            return hit.collider.gameObject.GetComponent<Tile>();
        }
        return null;
    }


    public List<Tile> GetNeighbours(Tile tile) {
        List<Tile> neighbours = new List<Tile>();
        for (int i = 0; i < xCount; i++) {
            for (int j = 0; j < zCount; j++) {
                if (tiles[i, j] == tile) {
                    if (i + 1 < xCount && j + 1 < zCount)
                        neighbours.Add(tiles[i + 1, j + 1]);
                    if (i > 0 && j + 1 < zCount)
                        neighbours.Add(tiles[i - 1, j + 1]);
                    if (i + 1 < xCount)
                        neighbours.Add(tiles[i + 1, j]);
                    if (i + 1 < xCount && j > 0)
                        neighbours.Add(tiles[i + 1, j - 1]);
                    if (j + 1 < xCount)
                        neighbours.Add(tiles[i, j + 1]);
                    if (i > 0 && j > 0)
                        neighbours.Add(tiles[i - 1, j - 1]);
                    if (j > 0)
                        neighbours.Add(tiles[i, j - 1]);
                    if (i > 0)
                        neighbours.Add(tiles[i - 1, j]);
                }
            }
        }
        return neighbours;
    }

}
