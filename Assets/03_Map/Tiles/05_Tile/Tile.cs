﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {
    public Map map;
    public Building building;
    public Unit currentBuilder;

    public void Start() {

        float intensity = 0;
        if (this.building) {
            intensity = 9;
        } else {
            List<Tile> tiles = map.GetNeighbours(this);
            foreach (Tile tile in tiles) {
                if (tile.building) {
                    intensity++;
                }
            }
        }
        intensity = intensity / 8f;
        Color color = new Color(0.3526442f, 0.134f, 0.4665165f, 1f);
        this.GetComponentInChildren<Renderer>().material.SetColor("_EmissionColor", color * (intensity*1.25f + 0.5f));
        print(intensity);
    }
}
