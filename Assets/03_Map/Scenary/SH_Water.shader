// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "SH_Water"
{
	Properties
	{
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_Lambda("Lambda", Range( -1 , 1)) = 0.05
		_Color0("Color 0", Color) = (0.12322,0.2909196,0.6698113,0)
		_Color1("Color 1", Color) = (0.8066038,0.8864842,1,0)
		_EmissiveStrength("Emissive Strength", Range( 0 , 2)) = 0
		_PanningSpeed("Panning Speed", Vector) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard alpha:fade keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color0;
		uniform float4 _Color1;
		uniform float _Lambda;
		uniform sampler2D _TextureSample0;
		uniform float2 _PanningSpeed;
		uniform float _EmissiveStrength;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 panner4 = ( 1.0 * _Time.y * _PanningSpeed + i.uv_texcoord);
			float clampResult18 = clamp( ( ( i.uv_texcoord.y - _Lambda ) + tex2D( _TextureSample0, panner4 ).r ) , 0.0 , 1.0 );
			float4 lerpResult10 = lerp( _Color0 , _Color1 , clampResult18);
			o.Albedo = lerpResult10.rgb;
			o.Emission = ( _EmissiveStrength * lerpResult10 ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16900
277;73;1204;640;2323.097;338.2272;1.209837;True;False
Node;AmplifyShaderEditor.TextureCoordinatesNode;5;-1796.318,17.96385;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;19;-1743.585,149.337;Float;False;Property;_PanningSpeed;Panning Speed;5;0;Create;True;0;0;False;0;0,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;3;-1373.318,-158.0362;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;9;-1414.631,-41.1326;Float;False;Property;_Lambda;Lambda;1;0;Create;True;0;0;False;0;0.05;0.05;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;4;-1513.318,87.96389;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.11,-0.3;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;8;-1104.632,-92.13258;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;2;-1287.318,92.96386;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;Create;True;0;0;False;0;06e542617ed40354d84a8c434f4f5472;06e542617ed40354d84a8c434f4f5472;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;6;-953.3187,8.963853;Float;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;11;-534.3325,-408.5023;Float;False;Property;_Color0;Color 0;2;0;Create;True;0;0;False;0;0.12322,0.2909196,0.6698113,0;1,0.06908593,0.01568627,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;12;-532.3325,-248.5023;Float;False;Property;_Color1;Color 1;3;0;Create;True;0;0;False;0;0.8066038,0.8864842,1,0;1,0.5803922,0.9055226,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;18;-366.333,17.49768;Float;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;10;-246.3325,-152.5023;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;14;-206.333,-294.5023;Float;False;Property;_EmissiveStrength;Emissive Strength;4;0;Create;True;0;0;False;0;0;0.5;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-49.33301,-221.5023;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;159,-133;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;SH_Water;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;True;0;False;Transparent;;Transparent;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;4;0;5;0
WireConnection;4;2;19;0
WireConnection;8;0;3;2
WireConnection;8;1;9;0
WireConnection;2;1;4;0
WireConnection;6;0;8;0
WireConnection;6;1;2;1
WireConnection;18;0;6;0
WireConnection;10;0;11;0
WireConnection;10;1;12;0
WireConnection;10;2;18;0
WireConnection;13;0;14;0
WireConnection;13;1;10;0
WireConnection;0;0;10;0
WireConnection;0;2;13;0
ASEEND*/
//CHKSM=673974CC6D0A0E1516B6874EC93A33EFF6116AB8